package io.github.talesm.storeorder;

import org.springframework.jdbc.core.JdbcTemplate;

public class ClientDao {
	private JdbcTemplate jdbcTemplate;

	public ClientDao(JdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}

	public boolean checkIfValid(Integer clientId) {
		if (clientId == null) {
			return false;
		}
		return jdbcTemplate.query("SELECT COUNT(*) FROM client WHERE id = ?", rs -> (rs.next() && rs.getInt(1) != 0),
				clientId);
	}
}
