package io.github.talesm.storeorder;

public class Item {
	private Integer productId;
	private Integer quantity;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer itemId) {
		this.productId = itemId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Item [productId=" + productId + ", quantity=" + quantity + "]";
	}
}
