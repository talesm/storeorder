package io.github.talesm.storeorder;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class OrderDao {
	private JdbcTemplate jdbcTemplate;

	public OrderDao(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public double evalPrice(long orderId) {
		return jdbcTemplate.query(
				"SELECT SUM(p.price*i.quantity) FROM order_product i JOIN product p ON p.id = i.product_id WHERE i.order_id = ?",
				rs -> rs.next() ? rs.getDouble(1) : 0, orderId);
	}

	public Long insert(Order order) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(connection -> {
			PreparedStatement ps = connection.prepareStatement("INSERT INTO \"order\"(client_id) VALUES (?)",
					new String[] { "id" });
			ps.setInt(1, order.getClientId());
			return ps;
		}, keyHolder);
		final Long id = (Long) keyHolder.getKey();
		List<Item> items = order.getItems();

		jdbcTemplate.batchUpdate("INSERT INTO order_product(order_id, product_id, quantity) VALUES (?, ?, ?)",
				new BatchPreparedStatementSetter() {

					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						Item item = items.get(i);
						ps.setLong(1, id);
						ps.setLong(2, item.getProductId());
						ps.setLong(3, item.getQuantity());
					}

					@Override
					public int getBatchSize() {
						return items.size();
					}
				});
		return id;
	}
}
