package io.github.talesm.storeorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {
	@Autowired
	JdbcTemplate jdbcTemplate;

	@RequestMapping("/send-order")
	public OrderResponse sendOrder(@RequestBody Order order) {
		// Validate client
		ClientDao clientDao = new ClientDao(jdbcTemplate);
		if (order.getClientId() == null || !clientDao.checkIfValid(order.getClientId())) {
			return OrderResponse.error("Invalid Client");
		}
		// Validate product list
		if (order.getItems() == null || order.getItems().isEmpty()) {
			return OrderResponse.error("Product list empty");
		}
		ProductDao productDao = new ProductDao(jdbcTemplate);
		if (!order.getItems().stream().allMatch(item -> item.getQuantity() != null && item.getQuantity() > 0
				&& productDao.checkIfValid(item.getProductId()))) {
			return OrderResponse.error("The order has one or more invalid product on it.");
		}
		OrderDao orderDao = new OrderDao(jdbcTemplate);
		long orderId = orderDao.insert(order);
		return OrderResponse.ok(orderDao.evalPrice(orderId));
	}
}
