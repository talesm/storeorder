package io.github.talesm.storeorder;

import org.springframework.jdbc.core.JdbcTemplate;

public class ProductDao {
	private JdbcTemplate jdbcTemplate;

	public ProductDao(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public boolean checkIfValid(int productId) {
		return jdbcTemplate.query("SELECT COUNT(*) FROM product WHERE id = ?", rs -> (rs.next() && rs.getInt(1) != 0),
				productId);
	}

}
