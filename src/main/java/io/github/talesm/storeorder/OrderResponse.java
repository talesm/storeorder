package io.github.talesm.storeorder;

public class OrderResponse {
	private boolean ok;

	private OrderResponse(boolean ok) {
		this.ok = ok;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	static public OrderResponse ok(double price) {
		return new OrderResponse(true) {
			@SuppressWarnings("unused")
			public double getPrice() {
				return price;
			}
		};
	}
	static public OrderResponse error(String explanation) {
		return new OrderResponse(false) {
			@SuppressWarnings("unused")
			public String getExplanation() {
				return explanation;
			}
		};
	}
}