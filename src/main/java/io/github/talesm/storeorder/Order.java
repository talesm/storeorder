package io.github.talesm.storeorder;

import java.util.List;

public class Order {
	private Integer clientId;
	private List<Item> items;

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Order [userId=" + clientId + ", items=" + items + "]";
	}
}
