/**
 * 
 */
$(document).ready(function() {
	$('form').submit(function(ev) {
		ev.preventDefault();
		let order = {
				clientId: +$('#clientId').val(),
				items: $('.item').map(function(){
					let $this = $(this);
					let productId = +$this.find('.productId').val() || 0;
					let quantity = +$this.find('.quantity').val() || 0;
					return (productId && quantity)? {
						productId,
						quantity,
					}: null;
				}).get(),
		};
		$.ajax({
			contentType : 'application/json',
			dataType : 'json',
			type : 'POST',
			data : JSON.stringify(order),
			url : '/send-order',
		}).done(function(data) {
			if (data.ok) {
				alert("Order added with success.\nYour final price is: $"+data.price);
			} else {
				alert(data.explanation)
			}
		}).fail(function(err) {
			alert("error processing the information")
		});
	});
});